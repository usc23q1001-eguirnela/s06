from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod
	def getFullName(self):
		pass

	def addRequest(self):
		pass

	def checkRequest(self):
		pass

	def addUser(self):
		pass

class Employee(Person):
	# Properties
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	# Setters
	def setFirstName(self, firstName):
		self._firstName = firstName

	def setLastName(self, lastName):
		self._lastName = lastName

	def setEmail(self, email):
		self._email = email

	def setDepartment(self, department):
		self._department = department

	# Getters
	def getFirstName(self):
		return (f"The first name of the employee is {self._firstName}")

	def getLastName(self):
		return (f"The last name of the employee is {self._lastName}")

	def getEmail(self):
		return (f"The email of the employee is {self._email}")

	def getDepartment(self):
		return (f"The department of the employee is {self._department}")

	# Functions
	def getFullName(self):
		return (f"{self._firstName} {self._lastName}")

	def addRequest(self):
		return ("Request has been added.")

	def checkRequest(self):
		return ("Request has been checked.")

	def addUser(self):
		return ("User has been added.")

	def login(self):
		return (f"{self._email} has logged in.")

	def logout(self):
		return (f"{self._email} has logged out.")


class TeamLead(Person):
	# Properties
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
		self.members = []

	# Setters
	def setFirstName(self, firstName):
		self._firstName = firstName

	def setLastName(self, lastName):
		self._lastName = lastName

	def setEmail(self, email):
		self._email = email

	def setDepartment(self, department):
		self._department = department

	# Getters
	def getFirstName(self):
		return (f"The first name of the team lead is {self._firstName}")

	def getLastName(self):
		return (f"The last name of the team lead is {self._lastName}")

	def getEmail(self):
		return (f"The email of the team lead is {self._email}")

	def getDepartment(self):
		return (f"The department of the team lead is {self._department}")

	def getMembers(self):
		return self.members

	# Functions
	def getFullName(self):
		return (f"{self._firstName} {self._lastName}")

	def addRequest(self):
		return ("Request has been added.")

	def checkRequest(self):
		return ("Request has been checked.")

	def addUser(self):
		return ("User has been added.")

	def login(self):
		return (f"{self._email} has logged in.")

	def logout(self):
		return (f"{self._email} has logged out.")

	def addMember(self, employee):
		self.members.append(employee)


class Admin(Person):
	# Properties
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	# Setters
	def setFirstName(self, firstName):
		self._firstName = firstName

	def setLastName(self, lastName):
		self._lastName = lastName

	def setEmail(self, email):
		self._email = email

	def setDepartment(self, department):
		self._department = department

	# Getters
	def getFirstName(self):
		return (f"The first name of the admin is {self._firstName}")

	def getLastName(self):
		return (f"The last name of the admin is {self._lastName}")

	def getEmail(self):
		return (f"The email of the admin is {self._email}")

	def getDepartment(self):
		return (f"The department of the admin is {self._department}")

	# Functions
	def getFullName(self):
		return (f"{self._firstName} {self._lastName}")

	def addRequest(self):
		return ("Request has been added.")

	def checkRequest(self):
		return ("Request has been checked.")

	def addUser(self):
		return ("User has been added.")

	def login(self):
		return (f"{self._email} has logged in.")

	def logout(self):
		return (f"{self._email} has logged out.")



class Request():
	# Properties
	def __init__(self, name, requester, dateRequested):
		super().__init__()
		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = "Null"

	# Setters
	def setName(self, name):
		self._name = name

	def setRequester(self, requester):
		self._requester = requester

	def setDateRequested(self, dateRequested):
		self._dateRequested = dateRequested

	def setStatus(self, status):
		self._status = status

	# Getters
	def getName(self):
		print(f"The name of the request is {self._name}")

	def getRequester(self):
		print(f"The requester of the request is {self._requester}")

	def getDateRequested(self):
		print(f"The date of the request is {self._dateRequested}")

	def getStatus(self):
		print(f"The status of the request is {self._status}")

	# Functions
	def updateRequest(self):
		return (f"Request {self._name} has been {self._status}.")

	def closeRequest(self):
		return (f"Request {self._name} has been {self._status}.")

	def cancelRequest(self):
		return (f"Request {self._name} has been {self._status}.")


# TestCase
emp1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
emp2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
emp3 = Employee("Lina", "Inverse", "ilina@mail.com", "Sales")
emp4 = Employee("Rylai", "Crestfall", "crylai@mail.com", "Sales")
admin1 = Admin("Raijin", "Thunderkeg", "traijin@mail.com", "Marketing")
tl1 = TeamLead("Raigor", "Stonehoof", "sraigor@mail.com", "Sales")
req1 = Request("New Hire Orientation", tl1, "27-Jul-2023")
req2 = Request("Laptop Repair", emp1, "1-Jul-2023")

assert emp1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Raijin Thunderkeg", "Full name should be Raijin Thunderkeg"
assert tl1.getFullName() == "Raigor Stonehoof", "Full name should be Raigor Stonehoof"
assert emp2.login() == "sjane@mail.com has logged in."
assert emp2.addRequest() == "Request has been added."
assert emp2.logout() == "sjane@mail.com has logged out."

tl1.addMember(emp3)
tl1.addMember(emp4)
for indivEmp in tl1.getMembers():
	print(indivEmp.getFullName())

assert admin1.addUser() == "User has been added."

req2.setStatus("closed")
print(req2.closeRequest())
